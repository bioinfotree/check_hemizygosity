# Copyright 2014 Michele Vidotto <michele.vidotto@gmail.com>

final.summary:
	ln -sf $(FINAL_SUMMARY) $@

scaffolds.fasta:
	ln -sf $(SCAFFOLD) $@

contigs.fasta:
	ln -sf $(CONTIG) $@

contigs.bed: final.summary
	./scaffolds2contigs.py <$< \
	| cut -f 1-5 \
	| grep -E 'contig_[0-9]+' >$@

contigs.fasta.new: contigs.bed scaffolds.fasta
	$(call load_modules); \
	bedtools getfasta -name -fi $^2 -bed <(cut -f 1-4 $<) -fo $@

.IGNORE: new.Vs.contigs.fasta
new.Vs.contigs.fasta: contigs.fasta.new contigs.fasta
	$(call load_modules); \
	sdiff -bBWs \
	<(fasta2tab <$< | bsort) \
	<(fasta2tab <$^2 | bsort) \
	| tee >$@

# chain	10	scaffold_9999	939	+	1	939	contig_113919	939	+	0	939	114026
# 939
chain: scaffolds.fasta contigs.bed
	translate -a <(cat $< | fasta_length) 1 <$^2 \
	| bawk 'BEGIN { N=0; } \
	!/^[\#+,$$]/ { \
	N++; \
		print "chain 1000 "$$1" "$$2" + "$$3" "$$4" "$$5" "$$6" + "0" "$$6" "N; \
		print $$6; \
		print ""; \
	} ' >$@

chain.new: final.summary
	$(call load_modules); \
	scaffolds2contigs -c <$< >$@

.IGNORE: new.Vs.chain
new.Vs.chain: chain.new chain
	sdiff -bBWs $< $^2 \
	| tee >$@

agp: final.summary
	$(call load_modules); \
	scaffolds2contigs -a <$< >$@

# jcvi python package is needed
.IGNORE: agp.veryfied
agp.veryfied: agp contigs.fasta scaffolds.fasta
	$(call load_modules); \
	python -m jcvi.formats.agp validate $< $^2 $^3 \
	|& tee >$@

ALL += contigs.bed \
	chain.new \
	agp \
	new.Vs.contigs.fasta \
	new.Vs.chain \
	agp.veryfied
