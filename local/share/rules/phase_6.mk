# Copyright 2015 Michele Vidotto <michele.vidotto@gmail.com>

extern ../phase_1/hemizygous.lst as HEMIZYGOUS_LST
extern ../phase_1/hemizygous.lst.fasta.gz as HEMIZYGOUS_FASTA_GZ
extern ../phase_1/not.hemizygous.lst as NOT_HEMIZYGOUS_LST
extern ../phase_5/validated.insertions.annot.seq as VALIDATED_INSERTIONS_ANNOT_SEQ
extern ../phase_5/validated.deletions.annot.seq as VALIDATED_DELETIONS_ANNOT_SEQ

# sensitive mode
RM_MODE ?= -qq

log:
	mkdir -p $@

contigs.fasta:
	ln -sf $(REFERENCE) $@

all.deletions.fasta.gz:
	ln -sf $(ALL_DELETIONS) $@

#-----------------------------------------------------------------------------

hemizygous.lst:
	ln -sf $(HEMIZYGOUS_LST) $@

hemizygous.lst.fasta:
	zcat $(HEMIZYGOUS_FASTA_GZ) >$@

not.hemizygous.lst:
	ln -sf $(NOT_HEMIZYGOUS_LST) $@

#-----------------------------------------------------------------------------

validated.insertions.annot.seq:
	ln -sf $(VALIDATED_INSERTIONS_ANNOT_SEQ) $@

validated.deletions.annot.seq:
	ln -sf $(VALIDATED_DELETIONS_ANNOT_SEQ) $@

final.summary:
	ln -sf $(FINAL_SUMMARY) $@

alignment.per_sequence_coverage.txt:
	ln -sf $(PER_SEQ_COV) $@ 


insertions.inner.contigs.fasta: validated.insertions.annot.seq
	bawk '!/^[\#+,$$]/ { \
		slice=$$18; \
		split($$18, seqs, "N"); \
		if ( ( $$16 == 1 ) && ( $$15 >= 0.25 && $$15 < 0.75 ) ) { \   * validated *
			n=0; \
			for ( i in seqs ) { \
				if ( seqs[i] != "" ) { \
							printf ">%i;%s;%s\n%s\n", n, $$1, $$2, seqs[i]; \
							n++; \
				} \
			} \
		} \
	}' $< >$@


deletions.inner.contigs.fasta: validated.deletions.annot.seq
	bawk '!/^[\#+,$$]/ { \
		slice=$$18; \
		split($$18, seqs, "N"); \
		if ( ( $$16 == 0 ) && ( $$15 >= 0.25 && $$15 < 0.75 ) ) { \   * not validated *
			n=0; \
			for ( i in seqs ) { \
				if ( seqs[i] != "" ) { \
							printf ">%i;%s;%s\n%s\n", n, $$1, $$2, seqs[i]; \
							n++; \
				} \
			} \
	} \
	}' $< >$@

# I try to mask the contigs of validated hemizygous insertions and not validated hemizygous deletions with, sequences of contigs with 1/2 coverage
.PRECIOUS: %.inner.contigs.fasta.ori.out
%.inner.contigs.fasta.ori.out: %.inner.contigs.fasta hemizygous.lst.fasta
	!threads
	$(call load_modules); \
	RepeatMasker \
	$(RM_MODE) \
	-no_is \
	-nolow \
	-pa $$THREADNUM \
	-lib $^2 \
	$<

%.inner.contigs.fasta.masked %.inner.contigs.fasta.cat.gz %.inner.contigs.fasta.out %.inner.contigs.fasta.tbl: %.inner.contigs.fasta.ori.out
	touch $@

#-----------------------------------------------------------------------------

.META: scaffolds.bed
	1	scaffold	scaffold_30
	2	contig_start	420149
	3	contig_end	426332
	4	contig_name	contig_2364
	5	mean_coverage	35.89
	6	median_coverage	33.00

# calculates coordinates of contigs inside scaffolds by parsing ALLPATHS-LG final.summary file
scaffolds.bed: final.summary alignment.per_sequence_coverage.txt
	scaffolds2contigs <$< \
	| cut -f 1-4 \
	| grep -E 'contig_[0-9]+' \
	| translate -a -v -e '0' <(select_columns 1 5 6 <$^2) 4 >$@

.META: contigs.overlapping.%.bed
	1       variant containing scaffolds	scaffold_0
	2       variant start	234241
	3       variant end	234270
	4       variant coordinates on the reference	chr14:27891748-27891777
	5       variant containing scaffolds	scaffold_0
	6       contig start	233243
	7       contig end	235317
	8       contig name	contig_9
	9       contig mean coverage	24.23
	10      contig median coverage	18.00
	11      overlapping length	29

# calculates overlapping between contigs and coordinates from blasts in
# validated.*.annot.seq file
contigs.overlapping.%.1bp.bed: scaffolds.bed validated.%.annot.seq
	$(call load_modules); \
	bawk '!/^[\#+,$$]/ { \
	if ( ( $$11 < $$12 ) ) print $$1, $$11, $$12, $$2; \
	}' <$^2 \
	| bedtools intersect \
	-bed \
	-wo \
	-a stdin \
	-b $< >$@

contigs.overlapping.%.1bp.found.bed: scaffolds.bed validated.%.annot.seq
	$(call load_modules); \
	bawk '!/^[\#+,$$]/ { \
	if ( ( $$11 < $$12 ) && ( $$16 == 1 ) ) print $$1, $$11, $$12, $$2; \
	}' <$^2 \
	| bedtools intersect \
	-bed \
	-wo \
	-a stdin \
	-b $< >$@

contigs.overlapping.insertions.1bp.found.het.bed: scaffolds.bed validated.insertions.annot.seq
	$(call load_modules); \
	bawk '!/^[\#+,$$]/ { \
	if ( ( $$11 < $$12 ) && ( $$16 == 1 ) && ( $$15 >= 0.25 && $$15 < 0.75 ) ) print $$1, $$11, $$12, $$2; \
	}' <$^2 \
	| bedtools intersect \
	-bed \
	-wo \
	-a stdin \
	-b $< >$@

contigs.overlapping.deletions.1bp.found.het.bed: scaffolds.bed validated.deletions.annot.seq
	$(call load_modules); \
	bawk '!/^[\#+,$$]/ { \
	if ( ( $$11 < $$12 ) && ( $$16 == 0 ) && ( $$15 >= 0.25 && $$15 < 0.75 ) ) print $$1, $$11, $$12, $$2; \
	}' <$^2 \
	| bedtools intersect \
	-bed \
	-wo \
	-a stdin \
	-b $< >$@

#-----------------------------------------------------------------------------

contigs.overlapping.%.90.bed: scaffolds.bed validated.%.annot.seq
	$(call load_modules); \
	bawk '!/^[\#+,$$]/ { \
	if ( ( $$11 < $$12 ) ) print $$1, $$11, $$12, $$2; \
	}' <$^2 \
	| bedtools intersect \
	-bed \
	-wo \
	-f 0.9 \
	-a stdin \
	-b $< >$@

contigs.overlapping.%.90.found.bed: scaffolds.bed validated.%.annot.seq
	$(call load_modules); \
	bawk '!/^[\#+,$$]/ { \
	if ( ( $$11 < $$12 ) && ( $$16 == 1 ) ) print $$1, $$11, $$12, $$2; \
	}' <$^2 \
	| bedtools intersect \
	-bed \
	-wo \
	-f 0.9 \
	-a stdin \
	-b $< >$@

contigs.overlapping.insertions.90.found.het.bed: scaffolds.bed validated.insertions.annot.seq
	$(call load_modules); \
	bawk '!/^[\#+,$$]/ { \
	if ( ( $$11 < $$12 ) && ( $$16 == 1 ) && ( $$15 >= 0.25 && $$15 < 0.75 ) ) print $$1, $$11, $$12, $$2; \
	}' <$^2 \
	| bedtools intersect \
	-bed \
	-wo \
	-f 0.9 \
	-a stdin \
	-b $< >$@


contigs.overlapping.deletions.90.found.het.bed: scaffolds.bed validated.deletions.annot.seq
	$(call load_modules); \
	bawk '!/^[\#+,$$]/ { \
	if ( ( $$11 < $$12 ) && ( $$16 == 0 ) && ( $$15 >= 0.25 && $$15 < 0.75 ) ) print $$1, $$11, $$12, $$2; \
	}' <$^2 \
	| bedtools intersect \
	-bed \
	-wo \
	-f 0.9 \
	-a stdin \
	-b $< >$@

#-----------------------------------------------------------------------------

# calculates how mutch contig are shared with hemizygous.lst
contigs.overlapping.insertions.hemizygous.%.tab: contigs.overlapping.insertions.%.bed hemizygous.lst
	comm \
	<(bsort $^2 \
	| uniq) \
	<(select_columns 8 <$< \
	| bsort \
	| uniq) \
	| bawk 'BEGIN { uniq1=uniq2=common; } !/^[\#+,$$]/ { \
	if ( $$1 != "" ) uniq1++; \
	if ( $$2 != "" ) uniq2++; \
	if ( $$3 != "" ) common++; \
	} END { print "$^2", "$<", "common"; \
	print uniq1, uniq2, common; }' >$@

# calculates how mutch contig are shared with not.hemizygous.lst
contigs.overlapping.insertions.not.hemizygous.%.tab: contigs.overlapping.insertions.%.bed not.hemizygous.lst
	comm \
	<(bsort $^2 \
	| uniq) \
	<(select_columns 8 <$< \
	| bsort \
	| uniq) \
	| bawk 'BEGIN { uniq1=uniq2=common; } !/^[\#+,$$]/ { \
	if ( $$1 != "" ) uniq1++; \
	if ( $$2 != "" ) uniq2++; \
	if ( $$3 != "" ) common++; \
	} END { print "$^2", "$<", "common"; \
	print uniq1, uniq2, common; }' >$@


#-----------------------------------------------------------------------------

contigs.overlapping.deletions.hemizygous.%.tab: contigs.overlapping.deletions.%.bed hemizygous.lst
	comm \
	<(bsort $^2 \
	| uniq) \
	<(select_columns 8 <$< \
	| bsort \
	| uniq) \
	| bawk 'BEGIN { uniq1=uniq2=common; } !/^[\#+,$$]/ { \
	if ( $$1 != "" ) uniq1++; \
	if ( $$2 != "" ) uniq2++; \
	if ( $$3 != "" ) common++; \
	} END { print "$^2", "$<", "common"; \
	print uniq1, uniq2, common; }' >$@

contigs.overlapping.deletions.not.hemizygous.%.tab: contigs.overlapping.deletions.%.bed not.hemizygous.lst
	comm \
	<(bsort $^2 \
	| uniq) \
	<(select_columns 8 <$< \
	| bsort \
	| uniq) \
	| bawk 'BEGIN { uniq1=uniq2=common; } !/^[\#+,$$]/ { \
	if ( $$1 != "" ) uniq1++; \
	if ( $$2 != "" ) uniq2++; \
	if ( $$3 != "" ) common++; \
	} END { print "$^2", "$<", "common"; \
	print uniq1, uniq2, common; }' >$@

#ALL += contigs.overlapping.deletions.hemizygous.1bp.tab \
	contigs.overlapping.deletions.hemizygous.1bp.found.tab \
	contigs.overlapping.deletions.hemizygous.1bp.found.het.tab \
	contigs.overlapping.insertions.hemizygous.1bp.tab \
	contigs.overlapping.insertions.hemizygous.1bp.found.tab \
	contigs.overlapping.insertions.hemizygous.1bp.found.het.tab \
	contigs.overlapping.deletions.hemizygous.90.tab \
	contigs.overlapping.deletions.hemizygous.90.found.tab \
	contigs.overlapping.deletions.hemizygous.90.found.het.tab \
	contigs.overlapping.insertions.hemizygous.90.tab \
	contigs.overlapping.insertions.hemizygous.90.found.tab \
	contigs.overlapping.insertions.hemizygous.90.found.het.tab \
	contigs.overlapping.deletions.not.hemizygous.1bp.tab \
	contigs.overlapping.deletions.not.hemizygous.1bp.found.tab \
	contigs.overlapping.deletions.not.hemizygous.1bp.found.het.tab \
	contigs.overlapping.insertions.not.hemizygous.1bp.tab \
	contigs.overlapping.insertions.not.hemizygous.1bp.found.tab \
	contigs.overlapping.insertions.not.hemizygous.1bp.found.het.tab \
	contigs.overlapping.deletions.not.hemizygous.90.tab \
	contigs.overlapping.deletions.not.hemizygous.90.found.tab \
	contigs.overlapping.deletions.not.hemizygous.90.found.het.tab \
	contigs.overlapping.insertions.not.hemizygous.90.tab \
	contigs.overlapping.insertions.not.hemizygous.90.found.tab \
	contigs.overlapping.insertions.not.hemizygous.90.found.het.tab

#-----------------------------------------------------------------------------

.META: contigs.overlapping.%.lst
	1	mean mean
	2	mean median
	3	mean stdev
	4	mean min
	5	mean max
	6	mean count
	7	median mean
	8	median median
	9	median stdev
	10	median min
	11	median max
	12	median count
	13	length mean
	14	length median
	15	length stdev
	16	length min
	17	length max
	18	length count

# calculates statistics about coverage and length of contigs that overlap the coordinates of variants
# versus contigs that not overlap and contigs belonging to hemizygous and not hemizygous lists
contigs.overlapping.%.lst: contigs.overlapping.%.bed alignment.per_sequence_coverage.txt hemizygous.lst not.hemizygous.lst
	filter_1col 1 \
	<(select_columns 8 <$< \
	| bsort \
	| uniq) <$^2 \
	| select_columns 5 6 2 \
	| stat_base --mean --median --stdev --min --max --count \
	| sed -e 's/^/overlapping\t/' >$@ \
	&& \
	filter_1col -v 1 \
	<(select_columns 8 <$< \
	| bsort \
	| uniq) <$^2 \
	| select_columns 5 6 2 \
	| stat_base --mean --median --stdev --min --max --count \
	| sed -e 's/^/non_overlapping\t/' >>$@ \
	&& \
	filter_1col 1 \
	<(bsort <$^3 \
	| uniq) <$^2 \
	| select_columns 5 6 2 \
	| stat_base --mean --median --stdev --min --max --count \
	| sed -e 's/^/hemizygous\t/' >>$@ \
	&& \
	filter_1col 1 \
	<(bsort <$^4 \
	| uniq) <$^2 \
	| select_columns 5 6 2 \
	| stat_base --mean --median --stdev --min --max --count \
	| sed -e 's/^/not.hemizygous\t/' >>$@

#ALL += contigs.overlapping.insertions.1bp.lst \
	 contigs.overlapping.insertions.1bp.found.lst \
	 contigs.overlapping.insertions.1bp.found.het.lst \
	 contigs.overlapping.deletions.1bp.lst \
	 contigs.overlapping.deletions.1bp.found.lst \
	 contigs.overlapping.deletions.1bp.found.het.lst \
	 contigs.overlapping.insertions.90.lst \
	 contigs.overlapping.insertions.90.found.lst \
	 contigs.overlapping.insertions.90.found.het.lst \
	 contigs.overlapping.deletions.90.lst \
	 contigs.overlapping.deletions.90.found.lst \
	 contigs.overlapping.deletions.90.found.het.lst

#-----------------------------------------------------------------------------

.META: contigs.overlapping.%.bed.struct
	1	variant name
	2	scaffold name
	3	contigs followed by mean and median coverage
	4	....

# calculates the order of the contigs that overlap the variants with their mean and median coverage
contigs.overlapping.%.bed.struct: contigs.overlapping.%.bed
	select_columns 4 5 8 9 10 <$< \
	| bsort -k 1,1 \
	| tr \\t ' ' \
	| sed -e 's/ /\t/' -e 's/ /\t/' -e 's/\t/ /' \   * transform second space to tab *
	| set_collapse -o -g $$'\t' 2 \
	| bawk '{ print length($$0), $$0;}' \   * print line length *
	| bsort -n -k1,1 \   * order line by length *
	| cut -f2- \
	| sed -e 's/ /\t/' >$@

#ALL += contigs.overlapping.insertions.1bp.bed.struct \
	 contigs.overlapping.insertions.1bp.found.bed.struct \
	 contigs.overlapping.insertions.1bp.found.het.bed.struct \
	 contigs.overlapping.deletions.1bp.bed.struct \
	 contigs.overlapping.deletions.1bp.found.bed.struct \
	 contigs.overlapping.deletions.1bp.found.het.bed.struct \
	 contigs.overlapping.insertions.90.bed.struct \
	 contigs.overlapping.insertions.90.found.bed.struct \
	 contigs.overlapping.insertions.90.found.het.bed.struct \
	 contigs.overlapping.deletions.90.bed.struct \
	 contigs.overlapping.deletions.90.found.bed.struct \
	 contigs.overlapping.deletions.90.found.het.bed.struct

.PHONY:
test:
	@echo


ALL +=  hemizygous.lst \
	not.hemizygous.lst \
	$(addprefix insertions,.inner.contigs.fasta.ori.out .inner.contigs.fasta.masked .inner.contigs.fasta.cat.gz .inner.contigs.fasta.out .inner.contigs.fasta.tbl) \
	$(addprefix deletions,.inner.contigs.fasta.ori.out .inner.contigs.fasta.masked .inner.contigs.fasta.cat.gz .inner.contigs.fasta.out .inner.contigs.fasta.tbl) \
	contigs.overlapping.insertions.hemizygous.1bp.found.het.tab \
	contigs.overlapping.insertions.not.hemizygous.1bp.found.het.tab \
	contigs.overlapping.deletions.hemizygous.1bp.found.het.tab \
	contigs.overlapping.deletions.not.hemizygous.1bp.found.het.tab \
	contigs.overlapping.insertions.1bp.found.het.lst \
	contigs.overlapping.deletions.1bp.found.het.lst \
	contigs.overlapping.insertions.1bp.found.het.bed.struct \
	contigs.overlapping.deletions.1bp.found.het.bed.struct


INTERMEDIATE += 

CLEAN += log
