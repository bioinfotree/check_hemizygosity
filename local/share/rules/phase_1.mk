# Copyright 2014 Michele Vidotto <michele.vidotto@gmail.com>

REFERENCE ?= 
PER_SEQ_COV ?= 
LOW_LIM ?= 
HIGH_LIM ?= 
# sensitive mode
RM_MODE ?= -s

log:
	mkdir -p $@

reference.fasta:
	ln -sf $(REFERENCE) $@

repeats.fasta:
	zcat $(DELETIONS) >$@


.META: alignment.per_sequence_coverage.txt
	1	sequence name	sequence_0
	2	lenght	200200
	3	covered bases	200189
	4	mapped bases	831425
	5	mean coverage over covered bases	73.258
	6	median coverage over covered bases	72.000
	7	stdev coverage over covered bases	21.983

alignment.per_sequence_coverage.txt:
	ln -sf $(PER_SEQ_COV) $@ 

# # compute coverage in dcr format
# .PRECIOUS: alignment.dcr.gz
# alignment.dcr.gz: log reference.fasta alignment.bam
# 	$(load_modules); \
# 	ns-compute-profile \
# 	--fasta $^2 \
# 	--input-file $^3 \
# 	--output-file alignment \
# 	--dcr-output
# 	3>&1 1>&2 2>&3 \
# 	| tee $</nt-compute-profile.$@.log
# 
# alignment.dcr: alignment.dcr.gz
# 	zcat $< >$@
# 
# coverage.txt: alignment.dcr
# 	$(load_modules); \
# 	alignment_coverage.py -d $< -o $@
# 
# chr.lst: reference.fasta
# 	fasta_length <$< \
# 	| bawk '!/^[\#+,$$^]/ { printf "%s\t0\t%i\n",$$1,$$2; }' >$@

FRACTIONS_LST = hemizygous.lst not.hemizygous.lst low.cov.lst uncovered.lst
hemizygous.lst: alignment.per_sequence_coverage.txt
	bawk '!/^[\#+,$$]/ { \
	if ( $$5 <= $(LOW_LIM) ) { print $$1>"low.cov$(suffix $@)"; } \
	else if ( $$5 > ( $(LOW_LIM) ) && $$5 < ( $(HIGH_LIM) ) ) { print $$1; } \
	else if ( $$5 >= ( $(HIGH_LIM) ) ) { print $$1>"not.$@"; } \
	}' $< >$@

not.hemizygous.lst: hemizygous.lst
	touch $@

low.cov.lst: hemizygous.lst
	touch $@

uncovered.lst: reference.fasta alignment.per_sequence_coverage.txt
	comm -23 \
	<(fasta2tab <$< | cut -f1 | bsort) \
	<(cut -f1 <$^2 | bsort) \
	>$@

FRACTIONS_FASTA_GZ = $(FRACTIONS_LST:.lst=.lst.fasta.gz)
%.lst.fasta.gz: reference.fasta %.lst
	$(call load_modules); \
	ns-fasta-extract --list $^2 --input $< \
	| gzip -c >$@

.META: contigs.stat
	1	sequences number
	2	mean
	3	median
	4	stdev
	5	min
	6	max
	7	total length

contigs.stat: hemizygous.lst.fasta.gz not.hemizygous.lst.fasta.gz low.cov.lst.fasta.gz uncovered.lst.fasta.gz
	>$@; \
	for FILE in $^; do \
		zcat $$FILE \
		| fasta_length \
		| stat_base --total --min --max --mean --median --stdev --precision=10 2 \
		| sed "s/^/$$(zcat $$FILE | fasta_count -s)\t/" \
		| sed "s/^/$$FILE\t/" >>$@; \
	done


.META: hemizygous.lst.chars
	1	number of chars
	2	IUPAC Code
	3	meaning
	4	complement

define DUAL_AMBIGUITY_CODES
M	A or C	K
R	A or G	Y
W	A or T	W
S	C or G	S
Y	C or T	R
K	G or T	M
endef

export DUAL_AMBIGUITY_CODES

FRACTIONS_CHARS = $(FRACTIONS_LST:.lst=.lst.chars)
%.lst.chars: %.lst.fasta.gz
	echo "$$DUAL_AMBIGUITY_CODES" \
	| translate -v -e '0' -a <( \
	zcat $< \
		| fasta_count -c ALL \
		| tr ',' \\n \
		| tr ':' \\t \
		| grep -E "M|R|W|S|Y|K" \
		| cut -f1,2 ) 1 \
	| bsort -k 1,1 \
	| select_columns 2 1 3 4 >$@





# query masked
FRACTIONS_MASKED = $(FRACTIONS_LST:.lst=.lst.fasta.masked)
# alignments
FRACTIONS_CAT = $(FRACTIONS_LST:.lst=.lst.fasta.cat.gz)
# output with header and wildcards
FRACTIONS_ORI_OUT = $(FRACTIONS_LST:.lst=.lst.fasta.ori.out)
# output with no header or wildcards
FRACTIONS_OUT = $(FRACTIONS_LST:.lst=.lst.fasta.out)
# summary file
FRACTIONS_TBL = $(FRACTIONS_LST:.lst=.lst.fasta.tbl)

# change to $(FRACTIONS_ORI_OUT) when multiple targets will be supported
.META: hemizygous.lst.fasta.ori.out
	1	Smith-Waterman score of the match
	2	% divergence = mismatches/(matches+mismatches)
	3	% of bases opposite a gap in the query sequence (deleted bp)
	4	% of bases opposite a gap in the repeat consensus (inserted bp)
	5	name of query sequence
	6	starting position of match in query sequence
	7	ending position of match in query sequence
	8	no. of bases in query sequence after the ending position of match
	9	C if match is with the Complement of the repeat consensus sequence, else +
	10	name of the matching interspersed repeat
	11	the class of the repeat
	12	no. of bases in (complement of) the repeat consensus sequence prior to beginning of the match (0 means that the match extended all the way to the end of the repeat consensus sequence)
	13	starting position of match in repeat consensus sequence
	14	ending position of match in repeat consensus sequence
	15	unique identifier for individual insertions 

.PRECIOUS: $(FRACTIONS_ORI_OUT)
%.lst.fasta.ori.out: repeats.fasta %.lst.fasta.gz
	!threads
	$(call load_modules); \
	RepeatMasker \
	$(RM_MODE) \
	-no_is \
	-nolow \
	-pa $$THREADNUM \
	-lib $< \
	$^2

%.lst.fasta.masked %.lst.fasta.cat.gz %.lst.fasta.out %.lst.fasta.tbl: %.lst.fasta.ori.out
	touch $@


FRACTIONS_REPEATS_CLASS = $(FRACTIONS_ORI_OUT:.lst.fasta.ori.out=.lst.class)
%.lst.class: %.lst.fasta.ori.out
	$(call load_modules); \
	if [ -s $< ]; then \
		sed 's/^ *//' <$< \
		| tr -s [:blank:] \\t \
		| cut -f 10 \
		| bawk '!/^[$$,\#]/ { \
		split($$0,a,"_"); \
		print a[1]; }' \
		| bsort \
		| uniq -c \
		| sed 's/^ *//' \
		| tr -s [:blank:] \\t \
		| select_columns 2 1 >$@; \
	else \
		touch $@; \
	fi;


.PHONY:
test:
	@echo $(FRACTIONS_FASTA_GZ)
	@echo $(FRACTIONS_ORI_OUT)
	@echo $(FRACTIONS_REPEATS_CLASS)
	@echo $(FRACTIONS_TBL)


ALL +=  reference.fasta \
	repeats.fasta \
	alignment.per_sequence_coverage.txt \
	$(FRACTIONS_LST) \
	$(FRACTIONS_FASTA_GZ) \
	$(FRACTIONS_CHARS) \
	$(FRACTIONS_ORI_OUT) \
	$(FRACTIONS_REPEATS_CLASS) \
	$(FRACTIONS_TBL) \
	contigs.stat



INTERMEDIATE += 

CLEAN += log \
	$(FRACTIONS_LST) \
	 alignment.per_sequence_coverage.txt \
	 $(FRACTIONS_MASKED) \
	 $(FRACTIONS_TBL) \
	 $(FRACTIONS_CAT) \
	 $(FRACTIONS_OUT)
