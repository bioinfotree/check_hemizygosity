# Copyright 2014 Michele Vidotto <michele.vidotto@gmail.com>

extern ../phase_1/hemizygous.lst as HEMIZYGOUS_LST
extern ../phase_1/not.hemizygous.lst as NOT_HEMIZYGOUS_LST



log:
	mkdir -p $@


reference.fasta:
	ln -sf $(REFERENCE) $@


coverage.hemizygous.lst:
	ln -sf $(HEMIZYGOUS_LST) $@

coverage.not.hemizygous.lst:
	ln -sf $(NOT_HEMIZYGOUS_LST) $@


.META: reference.IUPAC.bivalent.ac.lst
	1	sequence	contig_26
	2	length	5677
	3	M	A or C	K
	4	R	A or G	Y
	5	W	A or T	W
	6	S	C or G	S
	7	Y	C or T	R
	8	K	G or T	M


reference.IUPAC.bivalent.ac.lst: reference.fasta
	fasta2tab <$< \
	| bawk '!/^[\#+,$$]/ { \
	M = R = W = S = Y = K = 0; \
	M += gsub(/M|m/, "M", $$2); \
	R += gsub(/R|r/, "R", $$2); \
	W += gsub(/W|w/, "W", $$2); \
	S += gsub(/S|s/, "S", $$2); \
	Y += gsub(/Y|y/, "Y", $$2); \
	K += gsub(/K|k/, "K", $$2); \
	split($$1,a,"_"); \
	printf "%s_%s\t", a[1], a[2]; \
	print length($$2), M, R, W, S, Y, K; \
	}' >$@


hemizygous.IUPAC.bivalent.ac.lst: reference.IUPAC.bivalent.ac.lst coverage.hemizygous.lst
	translate -a $< 1 <$^2 >$@


not.hemizygous.lst: hemizygous.IUPAC.bivalent.ac.lst coverage.not.hemizygous.lst
	paste -s -d "\n" \
	<(bawk '!/^[\#+,$$]/ { \
	M = R = W = S = Y = K = 0; \
	M += $$3; \
	R += $$4; \
	W += $$5; \
	S += $$6; \
	Y += $$7; \
	K += $$8; \
	if ( (M == 0) && (R == 0) && (W == 0) && (S == 0) && (Y == 0) && (K == 0) ) { print $$1 >"hemizygous.lst"; } \
	else { print $$1; } \
	}' $<) \
	$^2 >$@


hemizygous.lst: not.hemizygous.lst
	touch $@


FRACTIONS_FASTA_GZ = not.hemizygous.lst.fasta.gz hemizygous.lst.fasta.gz
%.lst.fasta.gz: reference.fasta %.lst
	$(call load_modules); \
	ns-fasta-extract --list $^2 --input $< \
	| gzip -c >$@


.META: contigs.stat
	1	sequences number
	2	mean
	3	median
	4	stdev
	5	min
	6	max
	7	total length

contigs.stat: hemizygous.lst.fasta.gz not.hemizygous.lst.fasta.gz
	>$@; \
	for FILE in $^; do \
		zcat $$FILE \
		| fasta_length \
		| stat_base --total --min --max --mean --median --stdev --precision=10 2 \
		| sed "s/^/$$(zcat $$FILE | fasta_count -s)\t/" \
		| sed "s/^/$$FILE\t/" >>$@; \
	done


.PHONY:
test:
	@echo 


ALL +=  reference.fasta \
	reference.IUPAC.bivalent.ac.lst \
	hemizygous.lst \
	hemizygous.IUPAC.bivalent.ac.lst \
	$(FRACTIONS_FASTA_GZ) \
	contigs.stat

INTERMEDIATE += 

CLEAN += log \
	 not.hemizygous.lst \
	 coverage.not.hemizygous.lst \
	 coverage.hemizygous.lst
