# Copyright 2014 Michele Vidotto <michele.vidotto@gmail.com>

contigs.with.variants: variants
	cut -f1 $< \
	| bsort \
	| uniq -c \
	| sed 's/^ *//' \
	| tr -s ' ' \\t \
	| bsort -k 1,1n >$@

mutiple.contigs.with.variants: contigs.with.variants
	cut -f1 $< \
	| bsort -n \
	| uniq -c \
	| sed 's/^ *//' \
	| tr -s ' ' \\t \
	| select_columns 2 1 >$@


.PHONY:
test:
	@echo 


ALL +=  contigs.with.variants \
	mutiple.contigs.with.variants

INTERMEDIATE += 

CLEAN += 