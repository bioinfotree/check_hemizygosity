# Copyright 2014 Michele Vidotto <michele.vidotto@gmail.com>

extern ../phase_1/hemizygous.lst as HEMIZYGOUS_LST
extern ../phase_1/not.hemizygous.lst as NOT_HEMIZYGOUS_LST

RM_MODE ?= -s

log:
	mkdir -p $@

reference.tab:
	fasta2tab <$(REFERENCE) >$@

all.deletions.fasta.gz:
	ln -sf $(ALL_DELETIONS) $@

deletions.annotations.tab:
	unhead $(DELETIONS_ANNOTATIONS) \
	| select_columns 4 71 >$@

insertions.annotations.tab:
	unhead $(INSERTIONS_ANNOTATIONS) \
	| bawk '!/^[\#+,$$]/ { \
		print $$1":"$$2"-"$$3, $$59; \
	}' >$@

hemizygous.lst:
	ln -sf $(HEMIZYGOUS_LST) $@

not.hemizygous.lst:
	ln -sf $(NOT_HEMIZYGOUS_LST) $@


.META: validated.insertions validated.deletions
	1	contig
	2	insertion
	3	sx_del.start
	4	sx_del.end
	5	sx_sc.start
	6	sx_sc.end
	7	dx_del.start
	8	dx_del.end
	9	dx_sc.start
	10	dx_sc.end
	11	sx.margin
	12	dx.margin
	13	dist
	14	ins.dist
	15	allele_ratio
	16	found

validated.insertions:
	unhead $(VALIDATED_INSERTIONS_CONTIGS) \
	| select_columns 2 1 3 4 5 6 7 8 9 10 11 12 13 14 15 \
	| bawk 'function abs(v) { return (v < 0 ? -v : v) } !/^[\#+,$$]/ { \
	found=0; \
	if ( abs($$13) >= 1000 ) found=1; \   * found if dist >= 1000 *
	print $$0,found; \
	}' >$@

validated.deletions:
	unhead $(VALIDATED_DELETIONS_CONTIGS) \
	| select_columns 2 1 3 4 5 6 7 8 9 10 11 12 13 14 15 \
	| bawk 'function abs(v) { return (v < 0 ? -v : v) } !/^[\#+,$$]/ { \
	found=0; \
	if ( abs($$13) < 1000 ) found=1; \   * found if dist < 1000 *
	print $$0,found; \
	}' >$@



validated.%.annot.seq: reference.tab validated.% %.annotations.tab
	translate -k -a -d -j -f 1 $^2 1 <$< \
	| translate -k -a -d -j -f 1 $^3 2 \
	| bawk 'function abs(v) { return (v < 0 ? -v : v) } !/^[\#+,$$]/ { \
		slice=""; \
		start=$$12; \
		if ( $$12 > $$13 ) { start=$$13 }; \
		slice = substr($$18, start, abs($$12-$$13)); \
		print $$1, $$2, $$4, $$5, $$6, $$7, $$8, $$9, $$10, $$11, $$12, $$13, $$14, $$15, $$16, $$17, $$3, slice; \
	}' >$@



.META: deletions.supposed.LTR.tab
	1	variant name
	2	variant sequence on the reference
	3	variant name plus mapping contig
	4	variant sequence on the contig

# retrieve deletions grater then 150bp with the hope that they represents individual LTRs leaved after an 
# event of excision of a transposable element that has left one of the two.
# can be computed only for deletions, since for insertions we do not have a fasta file from the reference that
# encode the putative insertions
deletions.supposed.LTR.tab: validated.deletions.annot.seq all.deletions.fasta.gz
	bawk '!/^[\#+,$$]/ { \
		if ( length($$18) >= 150 ) { print $$2, $$1, $$17, $$18 }; \   * select deletions suposed to be single LTRs *
	}' $< \
	| bsort -k 1,1 \
	| translate -k -a -d -j -f 1 <(zcat $^2 | fasta2tab) 1 \
	| bawk '!/^[\#+,$$]/ { printf "%s;%s;ref\t%s\t%s;%s;var\t%s\n", $$1, $$4, $$2, $$1, $$3, $$5; }' >$@


.META: deletions.supposed.LTR.blast
	1	qseqid	chr1:2339872-2340372
	2	query len	168
	3	sseqid	scaffold_11
	4	subject len	5042
	5	pident	99.4
	6	alignment length	501
	7	mismatch	3
	8	gapopen	0
	9	qstart	1
	10	qend	501
	11	sstart	1575703
	12	send	1575203
	13	evalue	0
	14	bitscore	909

# blast the sequence of the reconstructed variant against then
# sequence of the same deletion in the reference
deletions.supposed.LTR.blast: deletions.supposed.LTR.tab
	$(call load_modules); \
	>$@; \
	while read LINE; do \
		blastn \
		-subject <(echo "$$LINE" | cut -f 1,2 | tab2fasta 2) \   * sequence on the reference *
		-query <(echo "$$LINE" | cut -f 3,4 | tab2fasta 2) \   * sequence on the assembly *
		-evalue 10e-06 \
		-outfmt '6 qseqid qlen sseqid slen pident length mismatch gapopen qstart qend sstart send evalue bitscore' \
		-max_target_seqs 10 \
		>>$@; \
	done <$<

# mask the sequence of the deletion from the reference
# with the sequence of the reconstructed variant in the assembly
deletions.supposed.LTR.masked: deletions.supposed.LTR.tab
	$(call load_modules); \
	>$@; \
	TMP_DIR="$$(mktemp -d XXXXtmp)"; \
	cd "$$TMP_DIR"; \
	while read LINE; do \
		TMP_LIB="$$(mktemp XXXX.fasta)"; \
		TMP_SBJ="$$(mktemp XXXX.fasta)"; \
		echo "$$LINE" | cut -f 3,4 | tab2fasta 2 >"$$TMP_LIB"; \
		echo "$$LINE" | cut -f 1,2 | tab2fasta 2 >"$$TMP_SBJ"; \
		RepeatMasker \
		$(RM_MODE) \
		-no_is \
		-nolow \
		-lib "$$TMP_LIB" \   * sequence on the assembly *
		"$$TMP_SBJ"; \
		if [ -s "$$TMP_SBJ.masked" ]; then \
			cat "$$TMP_SBJ.masked" >>../$@; \
		fi; \
		rm -r "$$TMP_LIB" "$$TMP_SBJ".*; \
	done <../$<; \
	cd ..; \
	rm -r "$$TMP_DIR"


# print the 100bp beckwards, the reconstructed sequence of the deletions and the 100bp forward of the variant
deletions.supposed.LTR.tsd: reference.tab validated.deletions.annot.seq
	translate -k -a -d -j -f 1 $^2 1 <$< \
	| bawk 'function abs(v) { return (v < 0 ? -v : v) } !/^[\#+,$$]/ { \
		backwards_slice=forward_slice=""; \
		start=$$11; \
		stop=$$12; \
		if ( $$11 > $$12 ) { start=$$12; stop=$$11 }; \
		forward_slice = substr($$19, stop, 100); \
		beckwards_slice = substr($$19, start-100, 100); \
		if ( length($$18) >= 150 ) { print $$1, $$2, start, stop, beckwards_slice, $$18, forward_slice; } \   * select deletions suposed to be single LTRs *
	}' \
	| bsort -k 1,1 >$@


# try to asses which are really reconstructed solo LTRs using simple
# distance constraints
deletions.assessed.LTR.blast: deletions.supposed.LTR.blast
	bawk '!/^[\#+,$$]/ { \
		query_length=$$2; \
		sbj_length=$$4; \
		aln_length=$$6; \
		a[1]=sstart=$$11; \
		a[2]=send=$$12; \
		asort(a); \
		min_a=a[1]; max_a=a[2]; \
		split($$1,b,";"); \
		split($$3,c,";"); \
		if ( ( aln_length / query_length ) >= 0.8  && ( min_a <= 50 || ( sbj_length - max_a ) <= 50 ) ) { \   * query aligns for >80% and it aligns at less then 50bp from each edge of the subject *
		print b[1]"_"b[3], b[2], $$2, c[1]"_"c[3], c[2], $$4, $$5, $$6, $$7, $$8, $$9, $$10, $$11, $$12, $$13, $$14; }; \
		delete a; \
	}' $< \
	| bsort -k 1,1 -k 16,16n \
	| tr \\t ' ' \   * transform all tabs into spaces *
	| sed 's/ /\t/' \   * transform first space into tab *
	| set_collapse -g $$'\t' 2 \
	| bawk '!/^[\#+,$$]/ { if ( NF==3 ) print $$0; }' \   * select subjects on with the query aligns exactly 2 times *
	| tr -s ' ' \\t >$@

# sobstitute slice with regexp of sequences describing letters and number example: A[5]T[6]G[7]C[3] N[100] A[5]T[6] N[1]
validated.%.struct.found: validated.%.annot.seq
	bawk '!/^[\#+,$$]/ { \
		slice=$$18; \
		gsub(/[ABCDGHKMRSTVWY]+/,"sequence ",slice); \
		gsub(/N{10,}/,"gap ",slice); \
		gsub(/N{1}/,"single_N ",slice); \
		if ( $$16 == 1 ) print $$1, $$2, $$3, $$4, $$5, $$6, $$7, $$8, $$9, $$10, $$11, $$12, $$13, $$14, $$15, $$16, $$17, slice; \
		else print $$1, $$2, $$3, $$4, $$5, $$6, $$7, $$8, $$9, $$10, $$11, $$12, $$13, $$14, $$15, $$16, $$17, slice >"$(basename $@).not.found"; \
	}' $< >$@



.META: validated.%.struct validated.%.struct.found validated.%.struct.not.found
	1	contig
	2	insertion
	3	sx_del.start
	4	sx_del.end
	5	sx_sc.start
	6	sx_sc.end
	7	dx_del.start
	8	dx_del.end
	9	dx_sc.start
	10	dx_sc.end
	11	sx.margin
	12	dx.margin
	13	dist
	14	ins.dist
	15	allele_ratio
	16	found
	17	annotation
	18	sequence_variant_structure

validated.%.struct.not.found: validated.%.struct.found
	touch $@


validated.%.struct: validated.%.struct.found validated.%.struct.not.found
	cat $^ >$@

.META: insertions.hemizygous insertions.not.hemizygous deletions.hemizygous deletions.not.hemizygous
	1	contig
	2	insertion
	3	sx_del.start
	4	sx_del.end
	5	sx_sc.start
	6	sx_sc.end
	7	dx_del.start
	8	dx_del.end
	9	dx_sc.start
	10	dx_sc.end
	11	sx.margin
	12	dx.margin
	13	dist
	14	ins.dist
	15	allele_ratio
	16	found
	17	sequence_variant_structure

insertions.hemizygous: hemizygous.lst validated.insertions.struct
	translate -k -a -d -j -f 1 $^2 1 <$< >$@

insertions.not.hemizygous: not.hemizygous.lst validated.insertions.struct
	translate -k -a -d -j -f 1 $^2 1 <$< >$@


deletions.hemizygous: hemizygous.lst validated.deletions.struct
	translate -k -a -d -j -f 1 $^2 1 <$< >$@

deletions.not.hemizygous: not.hemizygous.lst validated.deletions.struct
	translate -k -a -d -j -f 1 $^2 1 <$< >$@



define _SUBDIR_PREPARE_
	RULES_PATH="$$PRJ_ROOT/local/share/rules/$1"; \
	if [ -s $$RULES_PATH ] && [ -s $2 ]; then \
		mkdir -p $5; \
		cd $5; \
		ln -sf ../$2; \   * link makefile *
		ln -sf ../$3 insertions; \   * file *
		ln -sf ../$4 deletions; \   * file *
		ln -sf $$RULES_PATH rules.mk; \   * link rules *
		bmake; \
		cd ..; \
	fi
endef



PHASE_NAME_1 = phase_4.1.mk

all.variants: makefile validated.insertions.struct validated.deletions.struct
	$(call _SUBDIR_PREPARE_,$(PHASE_NAME_1),$<,$^2,$^3,$@)

hemizygous.variants: makefile insertions.hemizygous deletions.hemizygous
	$(call _SUBDIR_PREPARE_,$(PHASE_NAME_1),$<,$^2,$^3,$@)

not.hemizygous.variants: makefile insertions.not.hemizygous deletions.not.hemizygous
	$(call _SUBDIR_PREPARE_,$(PHASE_NAME_1),$<,$^2,$^3,$@)






.PHONY:
test:
	@echo 


ALL +=  hemizygous.lst \
	not.hemizygous.lst \
	validated.insertions \
	validated.deletions \
	all.variants \
	validated.insertions.struct.found \
	validated.insertions.struct.not.found \
	validated.insertions.struct \
	validated.deletions.struct.found \
	validated.deletions.struct.not.found \
	validated.deletions.struct \
	hemizygous.variants \
	not.hemizygous.variants \
	deletions.hemizygous \
	deletions.not.hemizygous \
	insertions.hemizygous \
	insertions.not.hemizygous \
	deletions.assessed.LTR.blast \
	deletions.supposed.LTR.masked


INTERMEDIATE += validated.insertions.annot.seq \
		validated.deletions.annot.seq


CLEAN += log \
	 not.hemizygous.lst \
	 reference.tab