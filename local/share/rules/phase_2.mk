# Copyright 2014 Michele Vidotto <michele.vidotto@gmail.com>

extern ../phase_1/hemizygous.lst.fasta.masked as HEMIZYGOUS_MASKED
extern ../phase_1/not.hemizygous.lst.fasta.masked as NOT_HEMIZYGOUS_MASKED
extern ../phase_1/low.cov.lst.fasta.masked as LOW_COV_MASKED
extern ../phase_1/uncovered.lst.fasta.masked as UNCOVERED_MASKED

OTHER_FRACTIONS_MASKED_PATH ?=  ../../sangiovese_vcr23_3_stvariants/phase_1/
FRACTIONS_MASKED ?= 



reference.fasta:
	ln -sf $(REFERENCE) $@


FRACTIONS_MASKED = hemizygous.lst.fasta.masked not.hemizygous.lst.fasta.masked low.cov.lst.fasta.masked uncovered.lst.fasta.masked

hemizygous.lst.fasta.masked:
	ln -sf $(HEMIZYGOUS_MASKED) $@

not.hemizygous.lst.fasta.masked:
	ln -sf $(NOT_HEMIZYGOUS_MASKED) $@

low.cov.lst.fasta.masked:
	ln -sf $(LOW_COV_MASKED) $@

uncovered.lst.fasta.masked:
	ln -sf $(UNCOVERED_MASKED) $@

# counts N already present in the reference
reference.N: reference.fasta
	fasta_count -c N <$< \
	| tr ':' \\t \
	| cut -f2 >$@


FRACTIONS_MASKED_COMBO = $(FRACTIONS_MASKED:.lst.fasta.masked=.lst.fasta.masked.combo)

%.lst.fasta.masked.combo: reference.N %.lst.fasta.masked
	paste \
	<(fasta2tab <$^2) \
	<(fasta2tab <$(OTHER_FRACTIONS_MASKED_PATH)/$^2) \
	| bawk -v ref_N=$$(cat $<) 'BEGIN { N_count=0; } \
	!/^[\#+,$$]/ { \
	if ( $$1 != $$3 ) { printf "[bawk $@]\tSequence header %s differs from %s at line %i, exit!\n",$$1,$$3,NR >"/dev/stderr"; exit 1; } \
	if ( length($$2) != length($$4) ) { printf "[bawk $@]\tSequence %s and %s differ in length at line %i, exit!\n",$$1,$$3,NR >"/dev/stderr"; exit 1; } \
	split($$2, chars1, ""); \
	split($$4, chars2, ""); \
	printf "%s\t",$$1; \
	for (i=1; i <= length($0); i++) { \
		# printf "\t%s\t%s", chars1[i], chars2[i]; \
		if ( chars1[i] == "N" || chars2[i] == "N" ) { \
			printf "N"; \
			N_count++; } \
		else { \
			printf "%s", chars2[i]; } \
		} \
	printf "\n"; \
	} END { print "bases masked",(N_count - ref_N) >"$@.N"; }' \   * remove N already present in the reference *
	| tab2fasta -s 2 >$@

FRACTIONS_MASKED_COMBO_N = $(FRACTIONS_MASKED:.lst.fasta.masked=.lst.fasta.masked.combo.N)
%.lst.fasta.masked.combo.N: %.lst.fasta.masked.combo
	touch $@

.PHONY:
test:


ALL +=  reference.N \
	$(FRACTIONS_MASKED) \
	$(FRACTIONS_MASKED_COMBO) \
	$(FRACTIONS_MASKED_COMBO_N)

INTERMEDIATE += 

CLEAN += 