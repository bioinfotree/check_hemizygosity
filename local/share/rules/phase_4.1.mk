# Copyright 2014 Michele Vidotto <michele.vidotto@gmail.com>



define _SUBDIR_PREPARE_
	RULES_PATH="$$PRJ_ROOT/local/share/rules/$1"; \
	if [ -s $$RULES_PATH ] && [ -s $2 ]; then \
		mkdir -p $4; \
		cd $4; \
		ln -sf ../$2; \   * link makefile *
		ln -sf ../$3 variants; \   * file *
		ln -sf $$RULES_PATH rules.mk; \   * link rules *
		bmake; \
		cd ..; \
	fi
endef

PHASE_NAME_1 = phase_4.2.mk

deletions.zygosity: makefile deletions
	$(call _SUBDIR_PREPARE_,$(PHASE_NAME_1),$<,$^2,$@)

insertions.zysosity: makefile insertions
	$(call _SUBDIR_PREPARE_,$(PHASE_NAME_1),$<,$^2,$@)



PHASE_NAME_2 = phase_4.3.mk

deletions.contigs: makefile deletions
	$(call _SUBDIR_PREPARE_,$(PHASE_NAME_2),$<,$^2,$@)

insertions.contigs: makefile insertions
	$(call _SUBDIR_PREPARE_,$(PHASE_NAME_2),$<,$^2,$@)


%tions.stats: %tions
	printf "total mapped variants:\t%i\n" "$$(wc -l $< | cut -d " " -f1)" >$@ \
	&& \
	printf "total found:\t%i\n" "$$(stat_base --total 16 <$<)" >>$@ \
	&& \
	BIN_FILE_PATH="$$PRJ_ROOT/local/share/rules/bin_file"; \
	if [ -s $$BIN_FILE_PATH ] ; then \
		printf "\nmodule(dist)_class\tcounts\n" >>$@; \
		bawk 'function abs(v) { return (v < 0 ? -v : v) } !/^[\#+,$$]/ { \
		print abs($$13); \
		}' $< \
		| binner -f $$PRJ_ROOT/local/share/rules/bin_file >>$@; \
	fi \
	&& \
	printf "\ncounts\telement_class\n" >>$@; \
	cut -f 17 $< \
	| bsort \
	| uniq -c \
	| sed 's/^ *//' \
	| tr -s ' ' \\t \
	| bsort -r -n -k 1,1 >>$@ \
	&& \
	printf "\ncounts\telement_class_found\n" >>$@; \
	bawk '!/^[\#+,$$]/ { \
	if ( $$16==1 ) { print $$17; } ; \
	}' $< \
	| bsort \
	| uniq -c \
	| sed 's/^ *//' \
	| tr -s ' ' \\t \
	| bsort -r -n -k 1,1 >>$@ \
	&& \
	printf "\ncounts\telement_class_not_found\n" >>$@; \
	bawk '!/^[\#+,$$]/ { \
	if ( $$16==0 ) { print $$17; } ; \
	}' $< \
	| bsort \
	| uniq -c \
	| sed 's/^ *//' \
	| tr -s ' ' \\t \
	| bsort -r -n -k 1,1 >>$@




.PHONY:
test:
	@echo 


ALL +=  deletions.zygosity \
	insertions.zysosity \
	deletions.contigs \
	insertions.contigs \
	insertions.stats \
	deletions.stats


INTERMEDIATE += 

CLEAN += 