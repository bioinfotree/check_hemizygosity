# Copyright 2014 Michele Vidotto <michele.vidotto@gmail.com>

extern ../phase_1/hemizygous.lst as HEMIZYGOUS_LST
extern ../phase_1/not.hemizygous.lst as NOT_HEMIZYGOUS_LST



log:
	mkdir -p $@

hemizygous.lst:
	ln -sf $(HEMIZYGOUS_LST) $@

not.hemizygous.lst:
	ln -sf $(NOT_HEMIZYGOUS_LST) $@

validated.insertions:
	ln -sf $(VALIDATED_INSERTIONS) $@

validated.deletions:
	ln -sf $(VALIDATED_DELETIONS) $@


validated.contigs.with.deletions: validated.deletions
	unhead $< \
	| cut -f2 \
	| bsort \
	| uniq -c \
	| sed 's/^ *//' \
	| tr -s ' ' \\t \
	| bsort -k 1,1n >$@

mutiple.validated.contigs.with.deletions: validated.contigs.with.deletions
	cut -f1 $< \
	| bsort -n \
	| uniq -c \
	| sed 's/^ *//' \
	| tr -s ' ' \\t \
	| select_columns 2 1 \
	| transpose >$@


validated.homozygous.deletions: validated.deletions
	unhead $< \
	| bawk '!/^[\#+,$$]/ { \
	if ( $$15 >= 0.75 ) { print $$0 } \
	}' >$@

validated.contigs.with.homozygous.deletions: validated.homozygous.deletions
	cut -f2 $< \
	| bsort \
	| uniq -c \
	| sed 's/^ *//' \
	| tr -s ' ' \\t \
	| bsort -k 1,1n >$@

multiple.validated.contigs.with.homozygous.deletions: validated.contigs.with.homozygous.deletions
	cut -f1 $< \
	| bsort -n \
	| uniq -c \
	| sed 's/^ *//' \
	| tr -s ' ' \\t \
	| select_columns 2 1 \
	| transpose >$@


validated.heterozygous.deletions: validated.deletions
	unhead $< \
	| bawk '!/^[\#+,$$]/ { \
	if ( $$15 >= 0.25 && $$15 < 0.75 ) { print $$0 } \
	}'>$@


validated.contigs.with.heterozygous.deletions: validated.homozygous.deletions
	cut -f2 $< \
	| bsort \
	| uniq -c \
	| sed 's/^ *//' \
	| tr -s ' ' \\t \
	| bsort -k 1,1n >$@


multiple.validated.contigs.with.heterozygous.deletions: validated.contigs.with.homozygous.deletions
	cut -f1 $< \
	| bsort -n \
	| uniq -c \
	| sed 's/^ *//' \
	| tr -s ' ' \\t \
	| select_columns 2 1 \
	| transpose >$@


#############################################################################

validated.insertions.hemizygous: hemizygous.lst validated.insertions
	translate -k -a -d -j -f 2 <(unhead $^2) 1 <$< >$@

validated.deletions.hemizygous: hemizygous.lst validated.deletions
	translate -k -a -d -j -f 2 <(unhead $^2) 1 <$< >$@

with.homozygous.deletion: validated.deletions.hemizygous
	bawk '!/^[\#+,$$]/ { \
	if ( $$15 >= 0.75 ) { print $$0 } \
	}' $< >$@

with.heterozygous.deletion: validated.deletions.hemizygous
	bawk '!/^[\#+,$$]/ { \
	if ( $$15 >= 0.25 && $$15 < 0.75 ) { print $$0 } \
	}' $< >$@

total.contigs.with.deletions: validated.deletions.hemizygous
	cut -f1 $< \
	| bsort \
	| uniq -c \
	| sed 's/^ *//' \
	| tr -s ' ' \\t \
	| bsort -k 1,1n >$@

contigs.with.%.deletion: with.%.deletion
	cut -f1 $< \
	| bsort \
	| uniq -c \
	| sed 's/^ *//' \
	| tr -s ' ' \\t \
	| bsort -k 1,1n >$@

mutiple.deletions.in.contigs.with.%.deletion: contigs.with.%.deletion
	cut -f1 $< \
	| bsort -n \
	| uniq -c \
	| sed 's/^ *//' \
	| tr -s ' ' \\t \
	| select_columns 2 1 \
	| transpose >$@

mutiple.deletions.in.total.contigs.with.deletion: total.contigs.with.deletions
	cut -f1 $< \
	| bsort -n \
	| uniq -c \
	| sed 's/^ *//' \
	| tr -s ' ' \\t \
	| select_columns 2 1 \
	| transpose >$@


.PHONY:
test:
	@echo 


ALL +=  hemizygous.lst \
	validated.insertions \
	validated.deletions \
	validated.insertions.hemizygous \
	validated.deletions.hemizygous \
	validated.contigs.with.deletions \
	mutiple.validated.contigs.with.deletions \
	validated.insertions.hemizygous \
	mutiple.validated.contigs.with.deletions \
	with.homozygous.deletion \
	with.heterozygous.deletion \
	total.contigs.with.deletions \
	contigs.with.homozygous.deletion \
	contigs.with.heterozygous.deletion \
	mutiple.deletions.in.contigs.with.homozygous.deletion \
	mutiple.deletions.in.contigs.with.heterozygous.deletion

INTERMEDIATE += 

CLEAN += log \
	 not.hemizygous.lst