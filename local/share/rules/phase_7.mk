# Copyright 2015 Michele Vidotto <michele.vidotto@gmail.com>

extern ../phase_4/validated.insertions.struct.not.found as NOT_VALIDATED_INSERTIONS

context prj/ltr_find

# sensitive mode
RM_MODE ?= -s

# parameters for tandem repeat finder
TRF_PARAM ?= 2 7 7 80 10 50 500 

log:
	mkdir -p $@

reference.tab:
	fasta2tab <$(REFERENCE) >$@

ltr.fasta:
	zcat $(TE_DB) \
	| fasta2tab \
	| bawk '!/^[$$,\#+]/ { if ($$1 ~ /\_LTR/) print $$1, toupper($$2); }' \
	| bsort \
	| tab2fasta 2 >$@

.META: validated.insertions.struct.not.found
	1	contig
	2	insertion
	3	sx_del.start
	4	sx_del.end
	5	sx_sc.start
	6	sx_sc.end
	7	dx_del.start
	8	dx_del.end
	9	dx_sc.start
	10	dx_sc.end
	11	sx.margin
	12	dx.margin
	13	dist
	14	ins.dist
	15	allele_ratio
	16	found
	17	annotation
	18	sequence_variant_structure

validated.insertions.struct.not.found:
	ln -sf $(NOT_VALIDATED_INSERTIONS) $@

50-1000.insetions: reference.tab validated.insertions.struct.not.found
	bawk '!/^[\#+,$$]/ { if ( $$13 >= 50 && $$13 < 1000 ) { print $$0; }; }' $^2 \
	| translate -k -a -d -j -f 1 $< 1 \
	| bawk 'function abs(v) { return (v < 0 ? -v : v) } !/^[\#+,$$]/ { \
		 slice=""; \
		 start=$$12; \
		 if ( $$12 > $$13 ) { start=$$13 }; \
		 slice = substr($$2, start, abs($$12-$$13)); \
		 print $$1, $$3, $$4, $$5, $$6, $$7, $$8, $$9, $$10, $$11, $$12, $$13, $$14, $$15, $$16, $$17, slice; \
	}' >$@

50-1000.insetions.fasta: 50-1000.insetions
	cut -f 2,17 $< \
	| bsort \
	| tab2fasta 2 >$@

.PRECIOUS: 50-1000.insetions.fasta.ori.out
50-1000.insetions.fasta.ori.out: ltr.fasta 50-1000.insetions.fasta
	!threads
	$(call load_modules); \
	RepeatMasker \
	$(RM_MODE) \
	-no_is \
	-nolow \
	-pa $$THREADNUM \
	-lib $< \
	$^2

50-1000.insetions.fasta.masked: 50-1000.insetions.fasta.ori.out
	touch $@

SPACE:=$(NOTHING) $(NOTHING)
50-1000.insetions.fasta.masked.trf: 50-1000.insetions.fasta.masked
	trf407b $< $(TRF_PARAM) -h -ngs -m >$@.tab \
	&& mv $<.$(subst $(SPACE),.,$(TRF_PARAM)).mask $@ \
	&& sed -i '/^$$/d' $@


# redirects sequences that has >=60 N to new file
define CLEAN_N
	cat $1 \
	| sed '/^$$/d' \
	| fasta2tab \
	| bawk '!/^[$$,\#+]/ \
	{ seqlen=length($$2); \
	line_tmp=$$0; \
	N_number=gsub("N","",$$2); \
	if ( N_number/seqlen < 0.6 ) {print line_tmp;} \
	else { print $$1 >"$(basename $(basename $@)).$2" } }' \
	| bsort \
	| tab2fasta 2
endef

# redirects sequences that has$(FASTA_CHUNCKS) \ >=80 N (due to repeats) to new file
50-1000.insetions.fasta.norepeat: 50-1000.insetions.fasta.masked.trf
	$(call CLEAN_N,$^,repeat) \
	>$@

50-1000.insetions.fasta.norepeat.stats: ltr.fasta 50-1000.insetions 50-1000.insetions.fasta.ori.out 50-1000.insetions.fasta.norepeat
	printf "LTRs:\t%i\n" "$$(fasta_count -s $< | cut -f1)" >$@ \
	&& \
	printf "50<= len(variants) <1000:\t%i\n" "$$(wc -l $^2 | cut -d " " -f1)" >>$@ \
	&& \
	printf "variants masked by LTRs:\t%i\n" "$$(wc -l $^3 | cut -d " " -f1)" >>$@ \
	&& \
	printf "variants masked >=60%% by LTRs and Tandem Repeats:\t%i\n" "$$(fasta_count -s $^4 | cut -f1)" >>$@


.PHONY:
test:
	@echo


ALL +=  validated.insertions.struct.not.found \
	50-1000.insetions.fasta.norepeat.stats


INTERMEDIATE += 

CLEAN += log
