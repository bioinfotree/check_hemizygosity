# Copyright 2014 Michele Vidotto <michele.vidotto@gmail.com>

.META: no.homozygous.snps
	1	chr_name
	2	windows_start
	3	windows_end

# select windows where there is less the 1 homozygous snp, i.e. heterozygous regions
no.homozygous.snps:
	$(call load_modules); \
	bawk '!/^[\#+,$$]/ { \
	if ( $$4 <= 5 ) print $$0; \
	}' <../../$(HOMO_SNPS) \
	| bedtools merge -d 2 -i stdin >$@


homozygous.variants: variants
	bawk '!/^[\#+,$$]/ { \
	if ( $$15 >= 0.75 ) { print $$0 } \
	}' <$< >$@
	


heterozygous.variants: variants
	bawk '!/^[\#+,$$]/ { \
	if ( $$15 >= 0.25 && $$15 < 0.75 ) { print $$0 } \
	}' <$< >$@


define _SUBDIR_PREPARE_
	RULES_PATH="$$PRJ_ROOT/local/share/rules/$1"; \
	if [ -s $$RULES_PATH ] && [ -s $2 ]; then \
		mkdir -p $4; \
		cd $4; \
		ln -sf ../$2; \   * link makefile *
		ln -sf ../$3 variants; \   * file *
		ln -sf $$RULES_PATH rules.mk; \   * link rules *
		bmake; \
		cd ..; \
	fi
endef


PHASE_NAME := phase_4.3.mk

homozygous.contigs: makefile homozygous.variants
	$(call _SUBDIR_PREPARE_,$(PHASE_NAME),$<,$^2,$@)

heterozygous.contigs: makefile heterozygous.variants
	$(call _SUBDIR_PREPARE_,$(PHASE_NAME),$<,$^2,$@)


.META: false.homozygous.variants false.validated.homozygous.variants
	1	chr_name
	2	variant_start
	3	variant_end
	4	zigosity_ratio
	5	validation_status
	6	chr_name
	7	windows_start
	8	windows_end
	9	overlapping bases between two features

false.homozygous.variants: no.homozygous.snps homozygous.variants
	$(call load_modules); \
	bawk '!/^[\#+,$$]/ { \
	split($$2,a,":"); \
	split(a[2],b,"-"); \
	print a[1], b[1], b[2], $$15, $$16; \   * chr_name, variant_start, variant_end, zigosity_ratio, validation_status *
	}' <$^2 \
	| bedtools intersect \
	-bed \
	-wo \
	-a stdin \   * compare every variant span, smaller *
	-b $< \   * with the snp window, match greater  *
	-f 0.95 \   * almost all variant span should be into the snp span *
	>$@

false.validated.homozygous.variants: false.homozygous.variants
	$(call load_modules); \
	bawk '!/^[\#+,$$]/ { if ( $$5==1 ) { print $$0; } }' $< >$@


%.variants.stats: %.variants false.homozygous.variants false.validated.homozygous.variants 
	printf "total mapped variants:\t%i\n" "$$(wc -l $< | cut -d " " -f1)" >$@ \
	&& \
	if [ $* == homozygous ]; then \
		printf "false positives:\t%i\n" "$$(wc -l $^2 | cut -d " " -f1)" >>$@; \
	fi && \
	printf "total found:\t%i\n" "$$(stat_base --total 16 <$<)" >>$@ \
	&& \
	if [ $* == homozygous ]; then \
		printf "false positives found:\t%i\n" "$$(wc -l $^3 | cut -d " " -f1)" >>$@; \
	fi && \
	BIN_FILE_PATH="$$PRJ_ROOT/local/share/rules/bin_file"; \
	if [ -s $$BIN_FILE_PATH ] ; then \
		printf "\nmodule(dist)_class\tcounts\n" >>$@; \
		bawk 'function abs(v) { return (v < 0 ? -v : v) } !/^[\#+,$$]/ { \
		print abs($$13); \
		}' $< \
		| binner -f $$PRJ_ROOT/local/share/rules/bin_file >>$@; \
	fi \
	&& \
	printf "\ncounts\telement_class\n" >>$@; \
	cut -f 17 $< \
	| bsort \
	| uniq -c \
	| sed 's/^ *//' \
	| tr -s ' ' \\t \
	| bsort -r -n -k 1,1 >>$@ \
	&& \
	printf "\ncounts\telement_class_found\n" >>$@; \
	bawk '!/^[\#+,$$]/ { \
	if ( $$16==1 ) { print $$17; } ; \
	}' $< \
	| bsort \
	| uniq -c \
	| sed 's/^ *//' \
	| tr -s ' ' \\t \
	| bsort -r -n -k 1,1 >>$@ \
	&& \
	printf "\ncounts\telement_class_not_found\n" >>$@; \
	bawk '!/^[\#+,$$]/ { \
	if ( $$16==0 ) { print $$17; } ; \
	}' $< \
	| bsort \
	| uniq -c \
	| sed 's/^ *//' \
	| tr -s ' ' \\t \
	| bsort -r -n -k 1,1 >>$@



.PHONY:
test:
	@echo $(basename $(notdir $(CURDIR)))


ALL +=  homozygous.variants \
	homozygous.variants.stats \
	heterozygous.variants \
	heterozygous.variants.stats \
	homozygous.contigs \
	heterozygous.contigs

INTERMEDIATE += 

CLEAN += 